// dependencies
var express = require('express'),
    logger = require('morgan'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    expressSession = require('express-session'),
    mongoose = require('mongoose'),
    hash = require('bcrypt-nodejs'),
    path = require('path'),
    passport = require('passport'),
    localStrategy = require('passport-local' ).Strategy;

// mongoose
//mongoose.createConnection('mongodb://localhost/brainstorming');
/*
mongoose.connect('mongodb://localhost/brainstorming', function(err) {
    if(err) {
        console.log('connection error', err);
    } else {
        console.log('connection successful');
    }
});
*/

// user schema/model
//var User = require('./models/user.js');

// create instance of express
var app = express();

/* binds css files to the node project */
app.use(express.static(path.join(__dirname, 'public')));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

// require routes
// if user logat
var routes = require('./routes/history.js');
/* setez homepage-ul */
app.use('/', routes);

/*
app.get('/new_cloud', function(req, res) {
  //res.redirect(path.join(__dirname, 'routes', 'new_cloud.js'));
  res.render('./views/new_cloud.ejs')
});
*/

// define middleware
/*
app.use(express.static(path.join(__dirname, '../client')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(require('express-session')({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

// configure passport
passport.use(new localStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());
*/

// routes
//app.use('/user/', routes);

/*
app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, '../client', 'index.html'));
});
*/

// error hndlers
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

app.use(function(err, req, res) {
  res.status(err.status || 500);
  res.end(JSON.stringify({
    message: err.message,
    error: {}
  }));
});

module.exports = app;

/*
angular.module('brainstormingApp', [
                                    'ngRoute'
                                    ])

  .config(function ($routeProvider, $locationProvider) {
    $routeProvider
      .when('/history', {
        templateUrl: './routes/history.js'
        //controller: 'MainCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });

    $locationProvider.html5Mode(true);
  });

/*  
angular.module('orderyourselfApp')

.controller('LoginCtrl', function ($scope, $http) {
    console.log('saysomething');
    $scope.hello = 'Hello world! LoginCtrl';
})

.controller('MainCtrl', function ($scope, $http) {
    console.log('shit is getting real');
    $http.get('/api/awesomeThings').success(function(awesomeThings) {
        $scope.awesomeThings = awesomeThings;
    });
});
*/