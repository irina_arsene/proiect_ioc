var express = require('express'),
    router = express.Router();

var sessions = [
  { title: 'session 1', deadline: '29.11.2015', tags: 'rosii, castraveti, ceapa'},
  { title: 'session 2', deadline: '29.11.2015', tags: 'rosii, castraveti, ceapa'},
  //{ title: 'session 3', deadline: '29.11.2015', tags: 'rosii, castraveti, ceapa'}
];

router.get('/', function(req, res) {
  res.render('history', {
    sessions: sessions,
  	});
});

router.get('/new_cloud', function(req, res) {
  res.render('new_cloud')
});

module.exports = router;

